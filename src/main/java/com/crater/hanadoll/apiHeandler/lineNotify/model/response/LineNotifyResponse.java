package com.crater.hanadoll.apiHeandler.lineNotify.model.response;

public record LineNotifyResponse(Integer status, String message) {
}
