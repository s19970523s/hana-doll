package com.crater.hanadoll.apiHeandler.lineNotify.handler;

import com.crater.hanadoll.apiHeandler.lineNotify.model.dto.LineNotifyDto;
import com.crater.hanadoll.apiHeandler.lineNotify.model.response.LineNotifyResponse;
import com.crater.hanadoll.exception.HttpClientException;
import com.crater.hanadoll.exception.TaskException;
import com.crater.hanadoll.exception.ThirdResponseException;
import com.crater.hanadoll.properties.LineNotifyProperties;
import com.crater.hanadoll.utils.HttpClientUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class LineNotifyHandler {
    private LineNotifyProperties lineNotifyProperties;

    public void callLineNotify(LineNotifyDto lineNotifyDto) {
        try {
            var authorization = lineNotifyDto.authorization();
            var notifyMessage = lineNotifyDto.notifyMessage();
            var response = HttpClientUtils.sendXFrom(lineNotifyProperties.getLineNotifyUrl(),
                    generateLineNotifyHeader(authorization), generateLineNotifyRequest(notifyMessage),
                    LineNotifyResponse.class);
            if (!response.status().equals(200)) throw new ThirdResponseException(response.status(), response.message());
        } catch (ThirdResponseException | HttpClientException e) {
            throw e;
        }
    }

    private Map<String, String> generateLineNotifyHeader(String authorization) {
        try {
            var header = new HashMap<String, String>();
            header.put("Authorization", "Bearer " + authorization);
            return header;
        } catch (Exception e) {
            throw new TaskException("generate line notify header fail", e);
        }
    }

    private Map<String, String> generateLineNotifyRequest(String lineNotifyMessage) {
        try {
            var request = new HashMap<String, String>();
            request.put("message", lineNotifyMessage);
            return request;
        } catch (Exception e) {
            throw new TaskException("generate Line Notify request fail", e);
        }
    }

    @Autowired
    public LineNotifyHandler setLineNotifyProperties(LineNotifyProperties lineNotifyProperties) {
        this.lineNotifyProperties = lineNotifyProperties;
        return this;
    }
}
