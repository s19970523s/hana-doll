package com.crater.hanadoll.apiHeandler.lineNotify.model.dto;

public record LineNotifyDto(String authorization, String notifyMessage) {
}
