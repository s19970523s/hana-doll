package com.crater.hanadoll.exception;

public class ThirdResponseException extends RuntimeException {
    private String responseCode;
    public ThirdResponseException() {
    }

    public ThirdResponseException(String message) {
        super(message);
    }

    public ThirdResponseException(String responseCode, String message) {
        super(message);
        this.responseCode = responseCode;
    }

        public ThirdResponseException(Integer responseCode, String message) {
        super(message);
        this.responseCode = String.valueOf(responseCode);
    }
}
