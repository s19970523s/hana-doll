package com.crater.hanadoll.exception;

public class WebCrawlerException extends RuntimeException{
    public WebCrawlerException(String message) {
        super(message);
    }

    public WebCrawlerException(String message, Throwable cause) {
        super(message, cause);
    }
}
