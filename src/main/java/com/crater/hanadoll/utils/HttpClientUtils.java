package com.crater.hanadoll.utils;

import com.crater.hanadoll.exception.HttpClientException;
import com.crater.hanadoll.exception.WebCrawlerException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class HttpClientUtils {
    private final static Logger log = LoggerFactory.getLogger(HttpClientUtils.class);
    public static String callToGetHtml(String url) {
        try {
            var client = HttpClient.newHttpClient();
            var request = HttpRequest.newBuilder(new URI(url)).GET().build();
            var response = client.send(request, HttpResponse.BodyHandlers.ofString());
            return response.body();
        } catch (URISyntaxException e) {
            throw new WebCrawlerException("generate url fail.", e);
        } catch (IOException | InterruptedException e) {
            throw new WebCrawlerException("call url fail", e);
        }
    }

    public static <T>T sendXFrom(String url, Map<String, String> headers, Map<String, String> formRequest, Class<T> responseClass) {
        try {
            log.info("url: " + url);
            headers.put("Content-Type", "application/x-www-form-urlencoded");
            var client = HttpClient.newHttpClient();
            var httpRequest = HttpRequest.newBuilder(new URI(url)).headers(generateHeader(headers))
                    .POST(HttpRequest.BodyPublishers.ofString(generateFormRequest(formRequest))).build();
            var response = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            var responseBody = response.body();
            return responseClass == String.class ? (T) responseBody : new Gson().fromJson(responseBody, responseClass);
        } catch (URISyntaxException e) {
            throw new HttpClientException("generate url fail.", e);
        } catch (Exception e) {
            throw new HttpClientException("call url fail", e);
        }
    }

    private static String[] generateHeader(Map<String, String> headers) {
        try {
            log.info("header info:");
            headers.forEach((k, v) -> log.info(k + " : " + v));
            var headerResult = new String[(headers.size() * 2)];
            var headerCount = 0;
            for (var k : headers.keySet()) {
                headerResult[headerCount ++] = k;
                headerResult[headerCount ++] = headers.get(k);
            }
            return headerResult;
        } catch (Exception e) {
            throw new HttpClientException("generate header fail", e);
        }
    }

    private static String generateFormRequest(Map<String, String> formRequest) {
        try {
            var form = new ArrayList<String>();
            for (String k : formRequest.keySet()) {
                form.add(k + "=" + formRequest.get(k).replaceAll("\\&", "和"));
            }
            var result = String.join("&", form);
            log.info("formRequest: " + result);
            return result;
        } catch (Exception e) {
            throw new HttpClientException("generate from request fail", e);
        }
    }
}
