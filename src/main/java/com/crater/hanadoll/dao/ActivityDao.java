package com.crater.hanadoll.dao;

import com.crater.hanadoll.dao.model.dto.ActivityData;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ActivityDao {
    void addNewData(ActivityData activityData);
    void addNotSameActivityNameData(ActivityData activityData);
    Optional<ActivityData> queryActivityDataById(Long id);
    Optional<List<ActivityData>> queryActivityDataByActivityDate(LocalDate startDate, LocalDate endDate);
    Optional<List<ActivityData>> queryInsertInToday();
}
