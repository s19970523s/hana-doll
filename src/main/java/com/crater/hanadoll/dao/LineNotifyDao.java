package com.crater.hanadoll.dao;

import com.crater.hanadoll.dao.model.dto.LineNotifyData;

import java.util.List;
import java.util.Optional;

public interface LineNotifyDao {
    List<LineNotifyData> queryAllAuthorization();
    Optional<String> queryAuthorizationByTarget(String targetName);
}
