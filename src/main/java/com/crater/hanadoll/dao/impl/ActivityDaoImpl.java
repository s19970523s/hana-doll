package com.crater.hanadoll.dao.impl;

import com.crater.hanadoll.dao.ActivityDao;
import com.crater.hanadoll.dao.mapper.ActivityMapper;
import com.crater.hanadoll.dao.model.dto.ActivityData;
import com.crater.hanadoll.dao.model.tablePojo.ActivityPojo;
import com.crater.hanadoll.exception.DbException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public class ActivityDaoImpl implements ActivityDao {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private ActivityMapper activityMapper;

    @Override
    public void addNewData(ActivityData activityData) {
        try {
            activityMapper.insert(new ActivityPojo(activityData.id(), activityData.activityName(),
                    activityData.location(), activityData.activityDate(), LocalDate.now()));
        } catch (Exception e) {
            throw new DbException("inert new data fail, activityData: " + activityData.toString(), e);
        }
    }

    @Override
    public void addNotSameActivityNameData(ActivityData activityData) {
        try {
            var activityPojos = activityMapper.query(
                    new ActivityPojo(null, null, null, null, null));
            if (activityPojos.stream().anyMatch(a -> activityData.activityName().equals(a.activityName()))) {
                log.info("found same activity name, will not insert, activity name is: " + activityData.activityName());

            } else {
                addNewData(activityData);
            }
        } catch (Exception e) {
            throw new DbException("insert not sane activity name data fail, activityData: " + activityData.toString(), e);
        }
    }

    @Override
    public Optional<ActivityData> queryActivityDataById(Long id) {
        try {
            var queryResults = activityMapper.query(
                    new ActivityPojo(id, null, null, null, null));
            if (queryResults.size() == 0) return Optional.empty();
            else {
                var queryResult = queryResults.get(0);
                return Optional.of(new ActivityData(queryResult.id(), queryResult.activityName(), queryResult.location(),
                        queryResult.activityDate()));
            }
        } catch (Exception e) {
            throw new DbException("query activity data fail, id: " + id, e);
        }
    }

    @Override
    public Optional<List<ActivityData>> queryActivityDataByActivityDate(LocalDate startTime, LocalDate endTime) {
        try {
            var queryResult = activityMapper.queryByActivityDate(startTime, endTime);
            if (queryResult.size() == 0) return Optional.empty();
            else return Optional.of(queryResult.stream()
                    .map(r -> new ActivityData(r.id(), r.activityName(), r.location(), r.activityDate())).toList());
        } catch (Exception e) {
            throw new DbException("query activity data between " + startTime + " to " + endTime, e);
        }
    }

    @Override
    public Optional<List<ActivityData>> queryInsertInToday() {
        var today = LocalDate.now();
        try {
            var queryResults = activityMapper
                    .query(new ActivityPojo(null, null, null, null, today));
            return queryResults.size() == 0 ? Optional.empty() : Optional.of(queryResults.stream().map(q ->
                    new ActivityData(q.id(), q.activityName(), q.location(), q.activityDate())).toList());
        } catch (Exception e) {
            throw new DbException("query today insert activity fail, today: " + today, e);
        }
    }

    @Autowired
    public ActivityDaoImpl setActivityMapper(ActivityMapper activityMapper) {
        this.activityMapper = activityMapper;
        return this;
    }
}
