package com.crater.hanadoll.dao.impl;

import com.crater.hanadoll.dao.LineNotifyDao;
import com.crater.hanadoll.dao.mapper.LineNotifyMapper;
import com.crater.hanadoll.dao.model.dto.LineNotifyData;
import com.crater.hanadoll.dao.model.tablePojo.LineNotifyTokenPojo;
import com.crater.hanadoll.exception.DbException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class LineNotifyDaoImpl implements LineNotifyDao {
    private LineNotifyMapper lineNotifyMapper;

    @Override
    public List<LineNotifyData> queryAllAuthorization() {
        try {
            var queryResult =
                    lineNotifyMapper.query(new LineNotifyTokenPojo(null, null, null, null));
            return queryResult.stream().map(this::pojoConverter2Dto).toList();
        } catch (Exception e) {
            throw new DbException("query all authorization fail", e);
        }
    }

    @Override
    public Optional<String> queryAuthorizationByTarget(String targetName) {
        try {
            var queryResult =
                    lineNotifyMapper.query(new LineNotifyTokenPojo(null, targetName, null, null));
            return queryResult.size() == 0 ? Optional.empty() : Optional.of(queryResult.get(0).authorization());
        } catch (Exception e) {
            throw new DbException("query authorization fail, target name:" + targetName, e);
        }
    }

    private LineNotifyData pojoConverter2Dto(LineNotifyTokenPojo lineNotifyTokenPojo) {
        return new LineNotifyData(lineNotifyTokenPojo.target(), lineNotifyTokenPojo.authorization());
    }

    @Autowired
    public LineNotifyDaoImpl setLineNotifyMapper(LineNotifyMapper lineNotifyMapper) {
        this.lineNotifyMapper = lineNotifyMapper;
        return this;
    }
}
