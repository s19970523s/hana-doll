package com.crater.hanadoll.dao.mapper;

import com.crater.hanadoll.dao.model.tablePojo.LineNotifyTokenPojo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface LineNotifyMapper {
    List<LineNotifyTokenPojo> query(LineNotifyTokenPojo queryCondition);
}
