package com.crater.hanadoll.dao.mapper;

import com.crater.hanadoll.dao.model.tablePojo.ActivityPojo;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface ActivityMapper {
    int insert(ActivityPojo activityData);

    List<ActivityPojo> query(ActivityPojo activityData);

    List<ActivityPojo> queryByActivityDate(LocalDate startDate, LocalDate endDate);
}
