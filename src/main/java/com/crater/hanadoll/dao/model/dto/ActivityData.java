package com.crater.hanadoll.dao.model.dto;

import java.time.LocalDate;

public record ActivityData(Long id, String activityName, String location, LocalDate activityDate) {
}

