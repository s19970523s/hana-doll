package com.crater.hanadoll.dao.model.tablePojo;

import java.time.LocalDate;

public record ActivityPojo(Long id, String activityName, String location, LocalDate activityDate,
                           LocalDate createDate) {
}

