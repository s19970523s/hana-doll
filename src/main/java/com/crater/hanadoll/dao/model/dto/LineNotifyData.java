package com.crater.hanadoll.dao.model.dto;

public record LineNotifyData(String target, String authorization) {
}
