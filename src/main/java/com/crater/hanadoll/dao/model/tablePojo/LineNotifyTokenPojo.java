package com.crater.hanadoll.dao.model.tablePojo;

import java.time.LocalDate;

public record LineNotifyTokenPojo(Long id, String target, String authorization, LocalDate createDate) {
}
