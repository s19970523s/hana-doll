package com.crater.hanadoll.plugin.impl;

import com.crater.hanadoll.exception.WebCrawlerException;
import com.crater.hanadoll.plugin.TakeActivityPlugin;
import com.crater.hanadoll.plugin.model.ActivityDto;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class CwtActivityPlugin implements TakeActivityPlugin {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @Override
    public List<ActivityDto> takeActivity() {
        try {
            var cwtHtml = takeCetHtml();
            return takeAllCwtActivityInfo(cwtHtml).stream()
                    .map(a -> new ActivityDto(takeActivityTime(a), takeActivityName(a), takeLocation(a))).toList();
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            throw new WebCrawlerException("take Cwt activity info fail", e);
        }
    }

    private Document takeCetHtml() {
        try {
            return Jsoup.connect("https://www.comicworld.com.tw/").get();
        } catch (IOException e) {
            throw new WebCrawlerException("take cwt info fail", e);
        }
    }

    private Elements takeAllCwtActivityInfo(Document html) {
        return html.getElementsByClass("activity_con");
    }

    private String takeActivityName(Element activityInfo) {
        return activityInfo.getElementsByClass("a_c_tit").get(0).text();
    }

    private LocalDateTime takeActivityTime(Element activityInfo) {
        var dateString = activityInfo.getElementsByClass("a_row").get(1).text();
        if (!dateString.startsWith("活動日期")) throw new WebCrawlerException("take cwt activity time fail");
        dateString = dateString.replace("活動日期", "").replaceAll("/.*", "")
                .replaceAll(" ", "");
        var year = Integer.parseInt(dateString.replaceAll("年.*", ""));
        var month = Integer.parseInt(dateString.replaceAll(".*年|月.*", ""));
        var day = Integer.parseInt(dateString.replaceAll(".*月|日", ""));
        return LocalDateTime.of(year, month, day, 0, 0);
    }

    private String takeLocation(Element activityInfo) {
        var location = activityInfo.getElementsByClass("a_row").get(2).text();
        if (!location.startsWith("活動地點")) throw new WebCrawlerException("take cwt activity location fail");
        return location.replaceAll("活動地點", "");
    }
}
