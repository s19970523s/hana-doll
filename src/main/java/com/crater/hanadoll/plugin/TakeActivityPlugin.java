package com.crater.hanadoll.plugin;

import com.crater.hanadoll.plugin.model.ActivityDto;

import java.util.List;

public interface TakeActivityPlugin {
    List<ActivityDto> takeActivity();
}
