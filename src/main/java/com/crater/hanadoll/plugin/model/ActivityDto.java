package com.crater.hanadoll.plugin.model;

import java.time.LocalDateTime;

public record ActivityDto(LocalDateTime activityTime, String activityName, String activityLocation) {

}
