package com.crater.hanadoll;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class HanaDollApplication {

    public static void main(String[] args) {
        SpringApplication.run(HanaDollApplication.class, args);
    }

}
