package com.crater.hanadoll.task;

import com.crater.hanadoll.apiHeandler.lineNotify.handler.LineNotifyHandler;
import com.crater.hanadoll.apiHeandler.lineNotify.model.dto.LineNotifyDto;
import com.crater.hanadoll.dao.ActivityDao;
import com.crater.hanadoll.dao.LineNotifyDao;
import com.crater.hanadoll.dao.model.dto.ActivityData;
import com.crater.hanadoll.dao.model.dto.LineNotifyData;
import com.crater.hanadoll.exception.TaskException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class SendLineNotifyTask {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private LineNotifyDao lineNotifyDao;
    private ActivityDao activityDao;
    private LineNotifyHandler lineNotifyHandler;

    @Scheduled(cron = "${task-run-time.send-line-notify-task}")
    public void sendLineNotify() {
        try {
            var authorizations = queryAllToken();
            var notifyMessage = generateNotifyMessage();
            for (String authorization : authorizations) {
                lineNotifyHandler.callLineNotify(new LineNotifyDto(authorization, notifyMessage));
            }
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
    }

    private String[] queryAllToken() {
        try {
            var queryResults = lineNotifyDao.queryAllAuthorization();
            return queryResults.stream().map(LineNotifyData::authorization).toArray(String[]::new);
        } catch (Exception e) {
            throw new TaskException("query all authorization fail", e);
        }
    }

    private String generateNotifyMessage() {
        try {
            var result = new StringBuilder();
            var newActivity = queryActivityCreatedToday();
            var upcomingActivity = queryUpcomingActivity();
            result.append("主人，").append("\n").append(generateNewActivityMessage(newActivity)).append("\n");
            result.append(generateUpcomingActivityMessage(upcomingActivity));
            return result.toString();
        } catch (TaskException e) {
            throw e;
        } catch (Exception e) {
            throw new TaskException("generate notify content fail", e);
        }
    }

    private Optional<List<ActivityData>> queryActivityCreatedToday() {
        try {
            return activityDao.queryInsertInToday();
        } catch (Exception e) {
            throw new TaskException("query activity info fail", e);
        }
    }

    private String generateNewActivityMessage(Optional<List<ActivityData>> activityDatas) {
        try {
            var result = new StringBuilder();
            if (activityDatas.isEmpty()) {
                result.append("沒有找到新的場次情報\n");
            } else {
                result.append("有新的場次情報喔，趕快檢查下面的訊息吧:\n");
                activityDatas.get().forEach(a -> result.append("場次名稱:").append(a.activityName()).append(", 地點: ")
                        .append(a.location()).append(", 日期: ")
                        .append(a.activityDate().format(DateTimeFormatter.ofPattern("yyyy年MM月dd日"))).append("\n"));
            }
            return result.toString();
        } catch (Exception e) {
            throw new TaskException("generate new activity message fail", e);
        }
    }

    private Optional<List<ActivityData>> queryUpcomingActivity() {
        var today = LocalDate.now();
        var endDate = today.plusDays(7);
        try {
            return activityDao.queryActivityDataByActivityDate(today, endDate);
        } catch (Exception e) {
            throw new TaskException("query upcoming activity, startDate: " + today + ", endDate: " + endDate, e);
        }
    }

    private String generateUpcomingActivityMessage(Optional<List<ActivityData>> upcomingActivity) {
        try {
            var result = new StringBuilder();
            if (upcomingActivity.isEmpty()) {
                result.append("沒有快要開始的場次喔\n");
            } else {
                result.append("有快要開始的場次喔，趕快準備吧:\n");
                upcomingActivity.get().forEach(a -> result.append("場次名稱:").append(a.activityName()).append(", 地點: ")
                        .append(a.location()).append(", 日期: ")
                        .append(a.activityDate().format(DateTimeFormatter.ofPattern("yyyy年MM月dd日"))).append("\n"));
            }
            return result.toString();
        } catch (Exception e) {
            throw new TaskException("generate upcoming activity message fail", e);
        }
    }

    @Autowired
    public SendLineNotifyTask setLineNotifyDao(LineNotifyDao lineNotifyDao) {
        this.lineNotifyDao = lineNotifyDao;
        return this;
    }

    @Autowired
    public SendLineNotifyTask setActivityDao(ActivityDao activityDao) {
        this.activityDao = activityDao;
        return this;
    }

    @Autowired
    public SendLineNotifyTask setLineNotifyHandler(LineNotifyHandler lineNotifyHandler) {
        this.lineNotifyHandler = lineNotifyHandler;
        return this;
    }
}
