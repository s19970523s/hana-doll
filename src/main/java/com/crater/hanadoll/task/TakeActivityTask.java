package com.crater.hanadoll.task;

import com.crater.hanadoll.dao.ActivityDao;
import com.crater.hanadoll.dao.model.dto.ActivityData;
import com.crater.hanadoll.exception.TaskException;
import com.crater.hanadoll.plugin.TakeActivityPlugin;
import com.crater.hanadoll.plugin.model.ActivityDto;
import com.crater.hanadoll.properties.TakeActivityProperties;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component
public class TakeActivityTask {
    private final Logger log = LoggerFactory.getLogger(TakeActivityTask.class);
    private ApplicationContext context;
    private TakeActivityProperties takeActivityProperties;
    private ActivityDao activityDao;

    @Scheduled(cron = "${task-run-time.take-activity-task}")
    public void takeActivity() {
        List<ActivityDto> allActivityDtos = new ArrayList<>();
        var willRunPlugins = getTakeActivityPluginBean();
        for (var willRunPlugin : willRunPlugins) {
            try {
                allActivityDtos.addAll(runPluginToGetActivityInfo(willRunPlugin));
            } catch (Exception e) {
                log.error(ExceptionUtils.getStackTrace(e));
            }
        }
        for (var activityData : allActivityDtos.stream().map(this::converter2ActivityData4Dao).toList()) {
            try {
                writeActivityInfoToDb(activityData);
            } catch (Exception e) {
                log.error(ExceptionUtils.getStackTrace(e));
            }
        }
    }

    private List<TakeActivityPlugin> getTakeActivityPluginBean() {
        try {
            var takeActivityPluginBeanNames = takeActivityProperties.getPluginNames().split(",");
            return Arrays.stream(takeActivityPluginBeanNames).map(s -> (TakeActivityPlugin) context.getBean(s)).toList();
        } catch (Exception e) {
            throw new TaskException("get takeActivityPlugin bean fail, please check plugin is exist, " +
                    "or take-activity.plugin-names setting have error", e);
        }
    }

    private List<ActivityDto> runPluginToGetActivityInfo(TakeActivityPlugin takeActivityPlugin) {
        try {
            return takeActivityPlugin.takeActivity();
        } catch (Exception e) {
            throw new TaskException("run activity plugin fail", e);
        }
    }

    private ActivityData converter2ActivityData4Dao(ActivityDto activityDto) {
        try {
            return new ActivityData(null, activityDto.activityName(), activityDto.activityLocation(),
                    activityDto.activityTime().toLocalDate());
        } catch (Exception e) {
            throw new TaskException("converter to activity data fail, activityDto data is: " + activityDto.toString());
        }
    }

    private void writeActivityInfoToDb(ActivityData activityData) {
        try {
            activityDao.addNotSameActivityNameData(activityData);
        } catch (Exception e) {
            throw new TaskException("insert activity fail, fail data is: " + activityData.toString(), e);
        }
    }

    @Autowired
    public TakeActivityTask setContext(ApplicationContext context) {
        this.context = context;
        return this;
    }

    @Autowired
    public TakeActivityTask setTakeActivityProperties(TakeActivityProperties takeActivityProperties) {
        this.takeActivityProperties = takeActivityProperties;
        return this;
    }

    @Autowired
    public TakeActivityTask setActivityDao(ActivityDao activityDao) {
        this.activityDao = activityDao;
        return this;
    }
}
