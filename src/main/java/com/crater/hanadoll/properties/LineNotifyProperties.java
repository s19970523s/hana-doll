package com.crater.hanadoll.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "line-notify")
public class LineNotifyProperties {
    private String lineNotifyUrl;

    public String getLineNotifyUrl() {
        return lineNotifyUrl;
    }

    public LineNotifyProperties setLineNotifyUrl(String lineNotifyUrl) {
        this.lineNotifyUrl = lineNotifyUrl;
        return this;
    }
}
