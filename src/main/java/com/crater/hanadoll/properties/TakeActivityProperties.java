package com.crater.hanadoll.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "take-activity")
public class TakeActivityProperties {
    private String pluginNames;

    public String getPluginNames() {
        return pluginNames;
    }

    public TakeActivityProperties setPluginNames(String pluginNames) {
        this.pluginNames = pluginNames;
        return this;
    }
}
