package com.crater.hanadoll.plugin.impl;

import org.junit.jupiter.api.Test;

public class CwtActivityPluginTest {
    @Test
    void dateTest() {
        var date = "2023年1月28/29日";
        date = date.replaceAll("/.*日", "日");
        System.out.println(date);
        System.out.println(date.replaceAll("年.*", ""));
        System.out.println(date.replaceAll(".*年|月.*", ""));
        System.out.println(date.replaceAll(".*月|日", ""));
    }
}
