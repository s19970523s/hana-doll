package com.crater.hanadoll.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class HttpClientUtilsTest {
    @Test
    void callToGetHtmlTest() throws IOException {
//        var cwtHtml = HttpClientUtils.callToGetHtml("https://www.comicworld.com.tw/");
//        System.out.println(cwtHtml);
        Document doc = Jsoup.connect("https://www.comicworld.com.tw/").get();
        var activityInfo = doc.getElementsByClass("activity_con");
        System.out.println(activityInfo.get(0));
    }

}
